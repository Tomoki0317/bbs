<?php
class StatusController extends Controller{
  protected $auth_actions = array('index','post');
  public function indexAction(){
    parent::loginFilter();
    $user = $this->session->get('user');
    $statuses = $this->db_manager->get('Post')->fetchAllPersonalArchivesByUserId($user['id']);
    $comments = $this->db_manager->get('Comment')->fetchAllCommentForThePostByUserId($user['id']);
    // $holdingcomment='';
    $errors = $this->session->get('errors');
    $post_id = $this->session->get('post_id');
    $holdingcomment = $this->session->get('holdingcomment');
    $this->session->set('errors',array());
    $this->session->set('holdingcomment',array());
    return $this->render(array(
      'user'=>$user,
      'statuses'=> $statuses,
      'comments'=> $comments,
      'errors'=>$errors,
      'post_id'=> $post_id,
      'holdingcomment'=> $holdingcomment,
      'search_words'=>array('category'=>'', 'from'=>'', 'to'=>''),
      '_token'=>$this->generateCsrfToken('status/comment'),
    ));
  }
  // return $this->render(array(
  // 'errors'=>$errors,
  // 'search_words'=>array('category'=>'', 'from'=>'', 'to'=>''),
  // 'post_id'=> $post_id,
  // 'user'=>$user,
  // 'statuses'=> $statuses,
  // 'holdingcomment'=> $comment,
  // 'comments'=> $post_comments,
  // '_token'=>$this->generateCsrfToken('status/comment'),),
  // 'index');

  public function userAction($params){
    parent::loginFilter();
    $user = $this->db_manager->get('User')->fetchByUserName($params['login_id']);
    if(!$user){
      $this->forward404();
    }
    $statuses = $this ->db_manager->get('Post')->fetchAllByUserId($user['id']);

    // $following = null;
    // if($this->session->isAuthenticated()){
    //   $my = $this->session->get('user');
    //   if($my['id'] !== $user['id']){
    //     $following = $this->db_manager->get('Following')->isFollowing($my['id'],$user['id']);
    //   }
    // }
    return $this->render(array(
      'user' => $user,
      'statuses' => $statuses,
      'following' => $following,
      '_token' => $this->generateCsrfToken('account/follow'),
    ));
  }


  public function showAction($params){
    parent::loginFilter();
    $status = $this->db_manager->get('Post')->fetchByIdAndUserName($params['id'],$params['login_id']);
    if(!$status){
      $this->forward404();
    }

    return $this->render(array('status' => $status));
  }






  public function commentAction(){
    parent::loginFilter();
    if (!$this->request->isPost()){
      $this->forward404();
    }
    $token = $this->request->getPost('_token');
    if (!$this->checkCsrfToken('status/comment', $token)){
      return $this->redirect('/');
    }
    $comment = $this->request->getPost('comment');
    $errors=array();

    if (!mb_strlen($comment)){
      $errors[]='コメントを入力してください         ';
    } else if (mb_strlen($comment)>500){
      $errors[]='コメントは500文字以内で入力してください　　';
    }
    //空白の場合は値を保持しない。
    if(mb_ereg_match("^(\s|　)+$", $comment)){
      $errors[]='コメントを入力してください';
      $post_id =  $this->request->getPost('post_id');
      $user = $this->session->get('user');
      $post_comments= $this->db_manager->get('Comment')->fetchAllCommentForThePostByUserId($user['id']);
      $statuses = $this->db_manager->get('Post')->fetchAllPersonalArchivesByUserId($user['id']);
      $this->session->set('errors', $errors);
      // $this->session->set('search_words', array('category'=>'', 'from'=>'', 'to'=>''));
      $this->session->set('post_id', $post_id);
      $this->session->set('holdingcomment', '');
      $this->session->set('_token', $this->generateCsrfToken('status/comment'));
      return $this->redirect('/#error'.$post_id);
    }


    $post_id =  $this->request->getPost('post_id');

    if(count($errors) === 0){
      $user = $this->session->get('user');

      $this->db_manager->get('Comment')->insert($user['id'], $post_id ,$comment);
      return $this->redirect('/#'.$post_id);
    }

    $user = $this->session->get('user');
    $post_comments= $this->db_manager->get('Comment')->fetchAllCommentForThePostByUserId($user['id']);
    $statuses = $this->db_manager->get('Post')->fetchAllPersonalArchivesByUserId($user['id']);



    $this->session->set('errors', $errors);
    // $this->session->set('search_words', array('category'=>'', 'from'=>'', 'to'=>''));
    $this->session->set('post_id', $post_id);
    $this->session->set('holdingcomment', $comment);
    $this->session->set('_token', $this->generateCsrfToken('status/comment'));
    return $this->redirect('/#error'.$post_id);
    // return $this->render(array(
    // 'errors'=>$errors,
    // 'search_words'=>array('category'=>'', 'from'=>'', 'to'=>''),
    // 'post_id'=> $post_id,
    // 'user'=>$user,
    // 'statuses'=> $statuses,
    // 'holdingcomment'=> $comment,
    // 'comments'=> $post_comments,
    // '_token'=>$this->generateCsrfToken('status/comment'),),
    // 'index');
  }


  // $errors=array();
  // $errors[] = 'ログインしてください';
  // $this->session->set('errors', $errors);
  // return $this->redirect('/account/signin');


  public function searchAction(){
    parent::loginFilter();

    $search_by_category= $this->request->getGet('category_search');
    $category = '%'.$search_by_category.'%';


    $from = $this->request->getGet('from_created_date_search');
    $to = $this->request->getGet('to_created_date_search');
    if($from==null){
      $fromTime=date("2000/01/01 00:00:00");
    }else{
      $fromTime = $from." 00:00:00";
    }
    if($to==null){
    $toTime=date("Y/m/d H:i:s")  ;
  }else{
    $toTime =$to." 23:59:59";
  }
    $statuses = $this ->db_manager->get('Post')
      ->fetchAllCommentsAfterSearching($category,$fromTime,$toTime);
    $user = $this->session->get('user');
    $comments = $this->db_manager->get('Comment')->fetchAllCommentForThePostByUserId($user['id']);
    $search_words=array('category'=>$search_by_category,'from'=>$from,'to'=>$to);
    return $this->render(array(
      'statuses' => $statuses,
      'user'=>$user,
      'search_words'=>$search_words,
      'comments'=> $comments,
    ),'index');
  }
}









 ?>
