<?php
class EditController extends Controller{
  public function indexAction(){
    parent::loginFilter();
    parent::authorityFilter();

    //Usersテーブルから全userのidを取得し配列に
    //0番目の人のidは何番...みたいな書き方

    // $_GET['edit_id']が数字かどうか　そうでなければリダイレクト
    // if(!preg_match($_GET['edit_id'])){
    //   return $this->redirect('/management');
    // }

    $all_user_id=$this->db_manager->get('User')->fetchAllUserIdFromUsers();
    $count=count($all_user_id)-1;
    $array=array();

    for($i=0; $i<=$count; $i++){
    $a_user_id=$all_user_id[$i];
    //このarrayにはuserIdのみ入っている
    $array[]=$a_user_id['id'];
    }
    $id = $_GET['edit_id'];
    if(!in_array($_GET['edit_id'],$array)){
      return $this->redirect('/management');
    }
    $editUser=$this->db_manager->get('User')->fetchEditUserById($id);
    $branch_data = $this->db_manager->get('Branch')->fetchBranchData();
    $department_data = $this->db_manager->get('Department')->fetchDepartmentData();
    $user = $this->session->get('user');
    return $this->render(array(
    'editUserData'=>$editUser,
    'branch_data'=>$branch_data,
    'user'=>$user,
    'edit_now_name'=>$editUser['name'],
    'department_data'=>$department_data,
    '_token' =>$this->generateCsrfToken('edit/edit'),));
  }






  public function editAction(){
    parent::loginFilter();
    // if ($this->session->isAuthenticated()) {
    //     return $this->redirect('/');
    // }
    if (!$this->request->isPost()) {
        $this->forward404();
    }

    $token = $this->request->getPost('_token');
    if (!$this->checkCsrfToken('edit/edit', $token)) {
        return $this->redirect('/edit');
    }
    $id= $this->request->getPost('id');
    $login_id = $this->request->getPost('login_id');
    $password = $this->request->getPost('password');
    $doubleCheckPassword = $this->request->getPost('doubleCheckPassword');
    $name = $this->request->getPost('name');
    $branch_id = $this->request->getPost('branch_id');
    $department_id = $this->request->getPost('department_id');
    $errors = array();

    if (!mb_strlen($login_id)) {
        $errors[] = 'ユーザIDを入力してください';
    } else if (!preg_match('/^[a-zA-Z0-9]{6,20}$/', $login_id)) {
        $errors[] = 'ユーザIDは半角英数字6 ～ 20 文字以内で入力してください';
    } else {
        $findUser = $this->db_manager->get('User')->fetchByLoginId($login_id);
        if($findUser != null && $id != $findUser['id']) {
          $errors[] = 'ユーザIDは既に使用されています';
        }
    }

    if ($password!=null && 6 > mb_strlen($password) || mb_strlen($password) > 20) {
        $errors[] = 'パスワードは半角文字6 ～ 20 文字以内で入力してください';
    }
    if($password!=$doubleCheckPassword){
        $errors[] = 'パスワードと確認用パスワードが一致しません';
    }

    if (!mb_strlen($name)) {
        $errors[] = '名前を入力してください';
    } else if (mb_strlen($name) > 10) {
        $errors[] = '名前は10 文字以内で入力してください';
    }
    if ($branch_id=='0'){
        $errors[] = '支店を選択してください';
    }
    if ($department_id=='0'){
        $errors[] = '役職を選択してください';
    }
    if($branch_id==2 && $department_id==1){
      $errors[] = '支店と役職の組み合わせが適切ではありません';
    }
    if($branch_id==3 && $department_id==1){
      $errors[] = '支店と役職の組み合わせが適切ではありません';
    }
    if($branch_id==4 && $department_id==1){
      $errors[] = '支店と役職の組み合わせが適切ではありません';
    }
    if($branch_id==2 && $department_id==2){
      $errors[] = '支店と役職の組み合わせが適切ではありません';
    }
    if($branch_id==3 && $department_id==2){
      $errors[] = '支店と役職の組み合わせが適切ではありません';
    }
    if($branch_id==4 && $department_id==2){
      $errors[] = '支店と役職の組み合わせが適切ではありません';
    }



    // パスワードなしで編集OK　ただし前のパスを保持
    // if (!mb_strlen($password)) {
    //     $errors[] = 'パスワードを入力してください';
    // } else if (6 > mb_strlen($password) || mb_strlen($password) > 20) {
    //     $errors[] = 'パスワードは6 ～ 20 文字以内で入力してください';
    // }



    // elseif($newpassword=null){
    //   $passward=
    //
    // }







    if (count($errors) === 0) {
      if($password==null){
        $this->db_manager->get('User')->updateWithoutPassword($id,$login_id,$name,$branch_id,$department_id);
      }else{
        $this->db_manager->get('User')->update($id,$login_id, $password,$name,$branch_id,$department_id);
      }
        // $this->session->setAuthenticated(true);

        // $user = $this->db_manager->get('User')->fetchByLoginId($login_id);
        // $this->session->set('user', $user);

        return $this->redirect('/management');
    }
    $editUserDataAfterError=array('id' => $id,'login_id'=>$login_id, 'password'=>'', 'name'=>$name, 'branch_id'=> $branch_id,'department_id'=> $department_id);
    $branch_data = $this->db_manager->get('Branch')->fetchBranchData();
    $department_data = $this->db_manager->get('Department')->fetchDepartmentData();
    $user = $this->session->get('user');
    $nowEditUser=$this->db_manager->get('User')->fetchEditUserById($id);
    $EditNowName=$nowEditUser['name'];
    return $this->render(array(
        'branch_data'=>$branch_data,
        'department_data'=>$department_data,
        'editUserData'=> $editUserDataAfterError,
        'edit_now_name'=>$EditNowName,
        'errors'    => $errors,
        'user'=>$user,
        '_token'    => $this->generateCsrfToken('edit/edit'),
    ), 'index');
  }
}
 ?>
