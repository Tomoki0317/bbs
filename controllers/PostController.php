<?php
class PostController extends Controller{
  protected $auth_actions = array('index','post');

  public function indexAction(){
    parent::loginFilter();
    return $this->render(array(
      '_token'    => $this->generateCsrfToken('post/post'),
      'postContentsAfterError'=>array('title'=>'', 'post'=>'', 'category'=>''),

    ));
  }





  public function postAction(){
    parent::loginFilter();
    if (!$this->request->isPost()){
      $this->forward404();
    }

    $token = $this->request->getPost('_token');
    if (!$this->checkCsrfToken('post/post', $token)){
      return $this->redirect('/');
    }
    $title = $this->request->getPost('title');
    $post = $this->request->getPost('post');
    // magic_quotes_gpcの値がONなら\削除
    // if ( get_magic_quotes_gpc() ) {
    //   $post = stripslashes( $post );
    // }
  //  $post = nl2br($post);

    $category = $this->request->getPost('category');
    $errors=array();

    //タイトル、投稿、カテゴリーは空白なら値保しない
    if (!mb_strlen($title)){
      $errors[]='タイトルを入力してください';
    } else if (mb_strlen($title)>30){
      $errors[]='タイトルは30文字以内で入力してください';
    }
    if(mb_ereg_match("^(\s|　)+$", $title)){
      $errors[]='タイトルを入力してください';
      $title='';
    }

    if (!mb_strlen($post)){
      $errors[]='投稿を入力してください';
    } else if (mb_strlen($post)>1000){
      $errors[]='投稿は1000文字以内で入力してください';
    }
    if(mb_ereg_match("^(\s|　)+$", $post)){
      $errors[]='投稿を入力してください';
      $post='';
    }

    if (!mb_strlen($category)){
      $errors[]='カテゴリーを入力してください';
    } else if (mb_strlen($category)>10){
      $errors[]='カテゴリーは10文字以内で入力してください';
    }
    if(mb_ereg_match("^(\s|　)+$", $category)){
      $errors[]='カテゴリーを入力してください';
      $category='';
    }


    if(count($errors) === 0){
      $user = $this->session->get('user');
      $this->db_manager->get('Post')->insert($user['id'], $title,$post,$category);
      return $this->redirect('/');
    }
    $user = $this->session->get('user');
    $postContentsAfterError=array('title'=>$title, 'post'=>$post, 'category'=>$category);
    $branch_data = $this->db_manager->get('Branch')->fetchBranchData();
    return $this->render(array(
    'errors'=>$errors,
    'postContentsAfterError'=>$postContentsAfterError,
    '_token' =>$this->generateCsrfToken('post/post'),),
    'index');
  }



  public function postdeleteAction(){
    parent::loginFilter();
    if (isset($_POST['delete'])) {
      $delete_id = $this->request->getPost('delete_id');
      $this->db_manager->get('Post')->deletePost($delete_id);
      $this->db_manager->get('Comment')->deleteCommentFromPostDelete($delete_id);
    }
    return $this->redirect('/');
  }

  public function commentdeleteAction(){
    parent::loginFilter();
    if (isset($_POST['delete'])) {
      $delete_id = $this->request->getPost('delete_id');
      $this->db_manager->get('Comment')->deleteComment($delete_id);
    }
    return $this->redirect('/');
  }
}
?>
