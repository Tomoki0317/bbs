<?php

/**
 * AccountController.
 *
 * @author Katsuhiro Ogawa <fivestar@nequal.jp>
 */
class AccountController extends Controller{
  protected $auth_actions = array('index', 'signout', 'follow');
  public function signupAction(){
      parent::loginFilter();
      $branch_data = $this->db_manager->get('Branch')->fetchBranchData();
      $department_data = $this->db_manager->get('Department')->fetchDepartmentData();
      return $this->render(array(
          'userData'=>null,
          'branch_data'=>$branch_data,
          'department_data'=>$department_data,
          '_token'    => $this->generateCsrfToken('account/signup'),
      ));
  }









  public function registerAction()
  {
      // if ($this->session->isAuthenticated()) {
      //     return $this->redirect('/account');
      // }

      if (!$this->request->isPost()) {
          $this->forward404();
      }

      $token = $this->request->getPost('_token');
      if (!$this->checkCsrfToken('account/signup', $token)) {
          return $this->redirect('/account/signup');
      }

      $login_id = $this->request->getPost('login_id');
      $password = $this->request->getPost('password');
      $doubleCheckPassword = $this->request->getPost('doubleCheckPassword');
      $name = $this->request->getPost('name');
      $branch_id = $this->request->getPost('branch_id');
      $department_id = $this->request->getPost('department_id');
      $errors = array();

      




      if (!mb_strlen($login_id)) {
          $errors[] = 'ユーザIDを入力してください';
      } else if (!preg_match('/^[a-zA-Z0-9]{6,20}$/', $login_id)) {
          $errors[] = 'ユーザIDは半角英数字6 ～ 20 文字以内で入力してください';
      } else if (!$this->db_manager->get('User')->isUniqueLoginId($login_id)) {
          $errors[] = 'ユーザIDは既に使用されています';
      }

      if (!mb_strlen($password)) {
          $errors[] = 'パスワードを入力してください';
      } else if (6 > mb_strlen($password) || mb_strlen($password) > 20) {
          $errors[] = 'パスワードは半角文字6 ～ 20 文字以内で入力してください';
      }
      if($password!=$doubleCheckPassword){
          $errors[] = 'パスワードと確認用パスワードが一致しません';
      }

      if (!mb_strlen($name)) {
          $errors[] = '名前を入力してください';
      } else if (mb_strlen($name) > 10) {
          $errors[] = '名前は10 文字以内で入力してください';
      }
      if ($branch_id=='0'){
          $errors[] = '支店を選択してください';
      }
      if ($department_id=='0'){
          $errors[] = '役職を選択してください';
      }
      if($branch_id==2 && $department_id==1){
        $errors[] = '支店と役職の組み合わせが適切ではありません';
      }
      if($branch_id==3 && $department_id==1){
        $errors[] = '支店と役職の組み合わせが適切ではありません';
      }
      if($branch_id==4 && $department_id==1){
        $errors[] = '支店と役職の組み合わせが適切ではありません';
      }
      if($branch_id==2 && $department_id==2){
        $errors[] = '支店と役職の組み合わせが適切ではありません';
      }
      if($branch_id==3 && $department_id==2){
        $errors[] = '支店と役職の組み合わせが適切ではありません';
      }
      if($branch_id==4 && $department_id==2){
        $errors[] = '支店と役職の組み合わせが適切ではありません';
      }


      if (count($errors) == 0) {
          $this->db_manager->get('User')->insert($login_id, $password,$name,$branch_id,$department_id);
          // $this->session->setAuthenticated(true);

          // $user = $this->db_manager->get('User')->fetchByLoginId($login_id);
          // $this->session->set('user', $user);

          return $this->redirect('/management');
      }

      $userDataAfterError=array('login_id'=>$login_id, 'password'=>$password, 'name'=>$name,'branch_id'=>$branch_id,'department_id'=>$department_id);
      $branch_data = $this->db_manager->get('Branch')->fetchBranchData();
      $department_data = $this->db_manager->get('Department')->fetchDepartmentData();
      return $this->render(array(
          'branch_data'=>$branch_data,
          'department_data'=>$department_data,
          'userData'=> $userDataAfterError,
          'errors'    => $errors,
          '_token'    => $this->generateCsrfToken('account/signup'),
      ), 'signup');
  }




  public function signinAction(){
      if ($this->session->isAuthenticated()) {
          return $this->redirect('/');
      }

      return $this->render(array(
          'login_id' => '',
          'password'  => '',
          '_token'    => $this->generateCsrfToken('account/signin'),
      ));
  }

  // public function signinFromLoginfilterAction(){
  //     if ($this->session->isAuthenticated()) {
  //         return $this->redirect('/');
  //     }
  //     $errors = 'ログインに失敗しました';
  //     return $this->render(array(
  //         'login_id' => '',
  //         'password'  => '',
  //         'errors'    => $errors,
  //         '_token'    => $this->generateCsrfToken('account/signin'),
  //     ),'signin');
  // }


  public function authenticateAction(){
    if (!$this->request->isPost()) {
        $this->forward404();
    }

    $token = $this->request->getPost('_token');
    if (!$this->checkCsrfToken('account/signin', $token)) {
        // return $this->redirect('/account/signin');
    }

    $login_id = $this->request->getPost('login_id');
    $password = $this->request->getPost('password');

    $errors = array();






    if (count($errors) === 0) {
        $user_repository = $this->db_manager->get('User');
        $user = $user_repository->fetchByLoginId($login_id);
        if($user['is_deleted']==1){
          $errors[] = 'このアカウントは現在使用できません';
        }else if(!$user || ($user['password'] !== $user_repository->hashPassword($password))
        ) {
            $errors[] = 'ログインに失敗しました';
        } else {
            $this->session->setAuthenticated(true);
            $this->session->set('user', $user);
            return $this->redirect('/');
        }
    }
    return $this->render(array(
        'login_id' => $login_id,
        'password'  => $password,
        'errors'    => $errors,
        '_token'    => $this->generateCsrfToken('account/signin'),
    ), 'signin');
  }


  public function signoutAction(){
    $this->session->clear();
    $this->session->setAuthenticated(false);
    return $this->redirect('/account/signin');
  }

//フォロー機能はいらない
//   public function followAction(){
//     echo 'start';
//     if(!$this->request->isPost()){
//       $this->forward404();
//     }
//
//     $following_name =$this->request->getPost('following_name');
//     if(!$following_name){
//       $this->forward404();
//     }
//     $token = $this->request->getPost('_token');
//     if(!$this->checkCsrfToken('account/follow',$token)){
//       return $this->redirect('/user/'. $following_name);
//     }
//     $follow_user = $this->db_manager->get('User')->fetchByUserName($following_name);
//     if(!$follow_user){
//       $this->forward404();
//     }
//     $user = $this->session->get('user');
//
//     $following_repository = $this->db_manager->get('Following');
//     if($user['id'] !== $follow_user['id'] && !$following_repository->isFollowing($user['id'], $follow_user['id'])){
//       $following_repository->insert($user['id'], $follow_user['id']);
//     }
//     return $this->redirect('/account');
//   }
}
?>
