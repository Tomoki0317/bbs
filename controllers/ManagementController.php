<?php
class ManagementController extends Controller{
  public function indexAction(){
    parent::loginFilter();
    parent::authorityFilter();
    $user = $this->session->get('user');
    $results = $this->db_manager->get('Management')->fetchAllUsersDataInnerJoinedByBranchesAndDepartments();
    return $this->render(array(
    'results'=>$results,
    'user'=>$user,
    '_token' =>$this->generateCsrfToken('management/is_deleted'),
  ));
 }

  public function managementAction(){
    parent::loginFilter();
    return $this->render(array(
    '_token' =>$this->generateCsrfToken('post/post'),));
  }

  public function is_deletedAction(){
    parent::loginFilter();
    if (!$this->request->isPost()){
      $this->forward404();
    }
    $token = $this->request->getPost('_token');
    if (!$this->checkCsrfToken('management/is_deleted', $token)){
      return $this->redirect('/');
    }
    $user_id = $this->request->getPost('user_id');
    $is_deleted = $this->request->getPost('is_deleted');
    $results = $this->db_manager->get('Management')->is_deletedUpdate($is_deleted,$user_id);
 return $this->redirect('/management');
  }
}



 ?>
