
<header>
  <div class="header-left">
    <a class="header-logo" href="<?php echo $base_url; ?>">　○△□ BBS</a>
  </div>
  <div class="header-right">
    <a class="headerbtn" href="<?php echo $base_url; ?>/">ホーム</a>
    <a class="headerbtn" href="<?php echo $base_url; ?>/management">ユーザー管理</a>
    <a class="headerbtn" href="<?php echo $base_url; ?>/account/signout">ログアウト</a>
  </div>
</header>




<h1 class="editpagetitle">ユーザー編集</h1>
<div class="editerrorarea">
  <?php if(isset($errors) && count($errors) >0): ?>
  <?php echo $this->render('errors', array('errors' => $errors)); ?>
  <?php endif; ?>
</div>




<form action="<?php echo $base_url; ?>/edit/edit" method="post">
  <input type="hidden" name="_token" value="<?php echo $this->escape($_token); ?>" >
  <input type="hidden" name="id" value="<?php  echo $this->escape($editUserData['id']); ?>" >

  <div class="editformbox">
    <span class="editbox-title"><?php echo $edit_now_name; ?>さんを編集中</span>
      <div class="editform">
        <table class="inputtable">
        <tbody>
          <tr>
            <th>ユーザID</th>
            <td>
              <input type="text" name="login_id" value="<?php echo $this->escape($editUserData['login_id']); ?>">
            </td>
          </tr>
          <tr>
            <th>パスワード</th>
            <td>
              <input type="password" name="password" placeholder="(変更する場合は入力)">
            </td>
          </tr>
          <tr>
            <th>確認用パスワード</th>
            <td>
              <input type="password" name="doubleCheckPassword" placeholder="(変更する場合は入力)" value="<?php //echo $this->escape($UserData['password']); ?>">
            </td>
          </tr>
          <tr>
            <th>名前</th>
            <td>
              <input type="text" name="name" value="<?php echo $this->escape($editUserData['name']); ?>">
            </td>
          </tr>
          <tr>
            <th>支店</th>
            <td>
              <?php if($editUserData['id']==$user['id']): ?>
                <?php foreach($branch_data as $branch_datum): ?>
                  <?php if($branch_datum['id']==$user['branch_id']): ?>
                    <input type="hidden" name="branch_id"  value=<?php echo $branch_datum['id'] ?>>
                    <input type="text" disabled value=<?php echo $branch_datum['name'] ?>>
                  <?php endif; ?>
                <?php endforeach; ?>
              <?php else: ?>
                <select name="branch_id">
                  <?php foreach($branch_data as $branch_datum): ?>
                    <?php if($branch_datum['id']==$editUserData['branch_id']): ?>
                      <option selected value=<?php echo $branch_datum['id'] ?>><?php echo $branch_datum['name'];?></option>
                    <?php else: ?>
                      <option value=<?php echo $branch_datum['id'] ?>><?php echo $branch_datum['name'];?></option>                  <?php endif; ?>
                    <?php endforeach; ?>
                </select>
              <?php endif; ?>
            </td>
          </tr>
            <tr>
            <th>役職</th>
            <td>
              <?php if($editUserData['id']==$user['id']): ?>
                <?php foreach($department_data as $department_datum): ?>
                  <?php if($department_datum['id']==$user['department_id']): ?>
                    <input type="hidden" name="department_id" value=<?php echo $department_datum['id'] ?>>
                    <input type="text" disabled value=<?php echo $department_datum['name'] ?>>
                  <?php endif; ?>
                <?php endforeach; ?>
              <?php else: ?>
            <select name="department_id">
              <?php foreach($department_data as $department_datum): ?>
                <?php if($department_datum['id']==$editUserData['department_id']): ?>
                  <option selected value=<?php echo $department_datum['id'] ?>><?php echo $department_datum['name'];?></option>
                <?php else: ?>
                  <option value=<?php echo $department_datum['id'] ?>><?php echo $department_datum['name'];?></option>
                <?php endif; ?>
              <?php endforeach; ?>
            </select>
            <?php endif; ?>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div class="doeditbtnarea">
    <p>
      <input class="doeditbtn" type="submit" value="編集">
    </p>
  </div>

</form>
