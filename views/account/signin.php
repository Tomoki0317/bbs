<!-- <?php $this->setLayoutVar('title','ログイン') ?> -->


<body class="signinpage">
  <h2>○△□ COMPANY BBS/掲示板</h2>
  <h1>ログイン</h1>

  <form class="" action="<?php echo $base_url; ?>/account/authenticate" method="post">
    <input type="hidden" name="_token" value="<?php echo $this->escape($_token); ?>">
<div class="errormessageareasignin">
  <?php if(isset($errors) && count($errors) >0): ?>
    <span class="letterred"><?php echo $this->render('errors',array('errors'=> $errors)); ?></span>
  <?php endif ?>
  <?php if(isset($_SESSION['errors']) && count($_SESSION['errors']) >0): ?>
    <span class="letterred"><?php echo $this->render('errors',array('errors'=> $_SESSION['errors'])); ?></span>
    <?php $_SESSION['errors'] = array(); ?>
  <?php endif ?>
</div>


    <table class="inputloginarea">
      <tbody>
        <tr >
          <th>ユーザID</th>
          <td>
            <input type="text" name="login_id" value="<?php echo $this->escape($login_id); ?>">
          </td>
        </tr>
        <tr>
          <th>パスワード</th>
          <td>
            <input type="password" name="password" >
          </td>
        </tr>
      </tbody>
    </table>

    <p>
      <input class="loginbtn" type="submit" name="" value="ログイン">
    </p>
  </form>
  </body>
  <div class="justblank2">

  </div>
  <footer>© 2018 Tomoki Fukuda</footer>
  <div class="freepik">
    <a  href='https://www.freepik.com/free-vector/workspace-in-cartoon-style_774622.htm'>Designed by Freepik</a>
  </div>
