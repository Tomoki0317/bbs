<!-- <?php
  $this->setLayoutVar('title','アカウント登録');
?> -->
<header>
  <div class="header-left">
    <a class="header-logo" href="<?php echo $base_url; ?>">　○△□ BBS</a>
  </div>
  <div class="header-right">
    <a class="headerbtn" href="<?php echo $base_url; ?>/">ホーム</a>
    <a class="headerbtn" href="<?php echo $base_url; ?>/management">ユーザー管理</a>
    <a class="headerbtn" href="<?php echo $base_url; ?>/account/signout">ログアウト</a>
  </div>
</header>






<h1 class="signuppagetitle">アカウント登録</h1>
<div class="signuperrorarea">
  <?php if(isset($errors) && count($errors) >0): ?>
    <ul class="error_list">
        <?php foreach ($errors as $error): ?>
        <li><?php echo $this->escape($error); ?></li>
      <?php endforeach; ?>
  </ul>
  <?php endif; ?>
</div>
  <form action="<?php echo $base_url; ?>/account/register" method="post">
    <input type="hidden" name="_token" value="<?php echo $this->escape($_token); ?>" >
  <div class="newsignupformbox">
    <span class="box-title">新規ユーザーを登録中</span>
      <div class="newsignupform">

        <table class="inputtable">
          <tbody>
            <tr>
              <th>ユーザID</th>
              <td>
                <input type="text" name="login_id" placeholder="(半角英数字6-20文字以内)" value="<?php echo $this->escape($userData['login_id']); ?>">
              </td>
            </tr>
            <tr>
              <th>パスワード</th>
              <td>
                <input type="password" name="password" placeholder="(半角英数字6-20文字以内)" value="<?php //echo $this->escape($userData['password']); ?>">
              </td>
            </tr>
            <tr>
              <th>確認用パスワード</th>
              <td>
                <input type="password" name="doubleCheckPassword" placeholder="(半角英数字6-20文字以内)" value="<?php //echo $this->escape($userData['password']); ?>">
              </td>
            </tr>
            <tr>
              <th>名前</th>
              <td>
                <input type="text" name="name" placeholder="(10文字以下)" value="<?php echo $this->escape($userData['name']); ?>">
              </td>
            </tr>
            <tr>
              <th>所属支店</th>
              <td>
                <select name="branch_id">
                <option value="0">選択してください</option>
                <?php foreach($branch_data as $branch_datum): ?>
                  <?php if($branch_datum['id']==$userData['branch_id']): ?>
                    <option selected value=<?php echo $branch_datum['id'] ?>><?php echo $branch_datum['name'];?></option>
                  <?php else: ?>
                  <option value=<?php echo $branch_datum['id'] ?>><?php echo $branch_datum['name'];?></option>
                  <?php endif; ?>
                <?php endforeach; ?>
                </select>
              </td>
            </tr>
            <tr>
              <th>役職</th>
              <td>
                <select name="department_id">
                <option value="0">選択してください</option>
                <?php foreach($department_data as $department_datum): ?>
                  <?php if($department_datum['id']==$userData['department_id']): ?>
                    <option selected value=<?php echo $department_datum['id'] ?>><?php echo $department_datum['name'];?></option>
                  <?php else: ?>
                  <option value=<?php echo $department_datum['id'] ?>><?php echo $department_datum['name'];?></option>
                  <?php endif; ?>
                <?php endforeach; ?>
                </select>
              </td>
            </tr>

          </tbody>
        </table>
    </div>

</div>


  <div class="dosignupbtnarea">
       <input class="dosignupbtn" type="submit" value="登録"><!--<input type="reset" value="リセット"> branch department値保持されてしまう-->
  </div>

</form>
