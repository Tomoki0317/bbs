<?php $this->setLayoutVar('title','ユーザー管理') ?>
<head>
  <script type="text/javascript">
  function doublecheck(){
    return confirm("変更しますか？");
}
  </script>
</head>


<header>
  <div class="header-left">
    <a class="header-logo" href="<?php echo $base_url; ?>">　○△□ BBS</a>
  </div>
  <div class="header-right">
    <a class="headerbtn" href="<?php echo $base_url; ?>/">ホーム</a>
    <a class="headerbtn" href="<?php echo $base_url; ?>/account/signup">新規ユーザ登録</a>
    <a class="headerbtn" href="<?php echo $base_url; ?>/account/signout">ログアウト</a>
  </div>
</header>
<h1 class="managementpagetitle" >ユーザー管理</h1>

<div class="managementpagebox">
  <!-- <span class="managementpagebox-title">ここにタイトル</span> -->
  <table class="managementtable">
    <thead>
      <tr>
        <th>ログインID</th>
        <th>名前</th>
        <th>支店</th>
        <th>役職</th>
        <th>現在の状態</th>
        <th>状態の変更</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
     <?php foreach ($results as $data):?>
         <tr>
             <td>
                 <?php echo $data["login_id"];?>
             </td>
             <td>
                 <?php echo $data["name"];?>
             </td>
             <td>
                 <?php echo $data["branches_name"];?>
             </td>
             <td>
                 <?php echo $data["departments_name"];?>
             </td>
             <td>
               <?php if($data["id"]==$user["id"]):?>
               <span class="lettergreen">ログイン中</span>
              <?php elseif($data["is_deleted"]==0): ?>
               <span class="letterblue">稼働中</span>
               <?php else: ?>
               <span class="letterred">停止中</span>
               <?php endif; ?>
             </td>
             <td>
               <?php if($data["id"]==$user["id"]):?>
               <?php else: ?>
               <form action="<?php echo $base_url; ?>/management/is_deleted" method="POST">
                 <input type="hidden" name="user_id" value="<?php echo $data["id"] ?>">
                 <input type="hidden" name="_token" value="<?php echo $this->escape($_token); ?>" >
                    <?php if($data["is_deleted"]==0): ?>
                      <input type="hidden" name="is_deleted" value="<?php echo $data["is_deleted"]+1 ?>">
                      <button class="stopbtn" type="submit" name="button" onClick="return doublecheck();">停止</button>
                    <?php else: ?>
                      <input type="hidden" name="is_deleted" value="<?php echo $data["is_deleted"]-1 ?>">
                    <button class="activebtn" type="submit" name="button" onClick="return doublecheck();">復活</button>
                    <?php endif; ?>
               </form>
               <?php endif; ?>
             </td>
             <td>
                <a class="editbtn" href="<?php echo $base_url; ?>/edit?edit_id=<?php echo $data["id"] ?>">ユーザー編集
             </td>
         </tr>
     <?php endforeach;?>
    </tbody>
  </table>

</div>
