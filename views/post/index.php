<?php $this->setLayoutVar('title','新規投稿') ?>
<!DOCTYPE html>
<html lang="" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
<body class="postpage">
    <header>
      <div class="header-left">
        <a class="header-logo" href="<?php echo $base_url; ?>">　○△□ BBS</a>
      </div>
      <div class="header-right">
        <a class="headerbtn" href="<?php echo $base_url; ?>/">ホーム</a>
        <a class="headerbtn" href="<?php echo $base_url; ?>/account/signout">ログアウト</a>
      </div>
    </header>
    <h1 class="posttitle">NEW POST</h1>
    <div class="newpost_from">
      <form action="<?php echo $base_url; ?>/status/post" method="post">
        <input type="hidden" name="_token" value="<?php echo $this->escape($_token); ?>">
        <div class="errormessageareapostpage">
          <?php if(isset($errors) && count($errors) >0): ?>
            <span class="letterred">  <?php echo $this->render('errors', array('errors' => $errors)); ?></span>
          <?php endif; ?>
        </div>

        <div class="post_content_input">
          <p>タイトル　　<input type="text" name="title" placeholder="(30文字以内)" value="<?php echo $this->escape($postContentsAfterError['title']); ?>"></p>
          <p>本文　　　　<textarea name="post" rows="8" cols="60" placeholder="　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　(1000文字以内)" ><?php echo $this->escape($postContentsAfterError['post']); ?></textarea></p>
          <p>カテゴリー　<input type="text" name="category" placeholder="(10文字以内)" value="<?php echo $this->escape($postContentsAfterError['category']); ?>"></p>
        </div>





          <!-- 値のリセットボタンの実装　はじめの状態にリダイレクト -->
          <div class="postgo">
            <input class="postgobtn" type="submit" value="投稿">
            <p><a href="<?php echo $base_url; ?>/post"><i class="fa fa-caret-right"></i>リセット</a></p>
          </div>
      </form>
    </div>


  </body>

</html>
