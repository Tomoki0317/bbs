<!-- <?php $this->setLayoutVar('title','ホーム') ?> -->
<head>
  <script type="text/javascript">
  function doublecheck(){
    return confirm("本当に削除しますか？");
}
  </script>
</head>

<header>



  <div class="container">
    <div class="header-left">
      <a class="header-logo" href="<?php echo $base_url; ?>">　○△□ BBS</a>
    </div>
    <div class="header-right">
      <?php if($user['department_id']==1): ?>
      <a class="headerbtn" href="<?php echo $base_url; ?>/management">管理者画面</a>
      <a class="headerbtn" href="<?php echo $base_url; ?>/account/signup">新規ユーザ登録</a>

      <?php endif; ?>
      <a class="headerbtn" href="<?php echo $base_url; ?>/account/signout">ログアウト</a>
    </div>
  </div>
</header>


<body>
  <div class="top-wrapper">
    <h1>ホーム</h1>
    <p><?php echo "ようこそ　".$user['name']."さん" ?></p>
    <p>Let's share your idea with us</p>
    <a class="gotonewpostpage" href="<?php echo $base_url; ?>/post">新規投稿</a>
  </div>




  <div id="nav-drawer">
      <input id="nav-input" type="checkbox" class="nav-unshown">
      <label id="nav-open" for="nav-input">
        <div class="search_icon">
          <i class="fas fa-search">検索</i>
        </div>

      </label>
      <label class="nav-unshown" id="nav-close" for="nav-input"></label>
      <div id="nav-content">
        <div class="search_box">
          <br>
          <form class="search" action="<?php echo $base_url; ?>/status/search" method="get">
            <?php if(isset($_SESSION['errors']) && count($_SESSION['errors']) >0): ?>
              <?php echo $this->render('errors',array('errors'=> $_SESSION['errors'])); ?>
              <?php $_SESSION['errors'] = array(); ?>
            <?php endif ?>
            <?php if($search_words['category'] == '' && $search_words['from'] == '' && $search_words['to'] == ''):?>
              <?php echo "" ?>
            <?php else: ?>
              <?php echo "以下の検索結果です" ?>
            <?php endif; ?>
            <br>
            <p>カテゴリー検索</p>
            <p><input type="text" name="category_search" value="<?php echo $this->escape($search_words['category']); ?>" ></p>
            <p>日付検索</p>
            <p><input type="date" name="from_created_date_search" value="<?php echo $this->escape($search_words['from']); ?>" ></p>
            <p><input type="date" name="to_created_date_search" value="<?php echo $this->escape($search_words['to']); ?>" ></p>
            <p><input type="submit" value="検索"></p>
          </form>
          <form action="<?php echo $base_url; ?>" method="post">
              <button type="submit" name="button">元に戻す</button>
          </form>
      </div>
  </div>




    <br>
    <div class="serchnothing">
      <?php if($statuses==null): ?>
        <p>表示できる投稿はありません</p>
      <?php endif; ?>
    </div>


    <br>
  </div>


  <div class="statuses">
    <?php foreach($statuses as $status): ?>
      <div class="status">
        <div class="post_content">
          <p><?php echo $this->escape($status['name']); ?>さんからの投稿</p>
          <p><span class="letterlittlesmall">タイトル：</span>
            <?php echo $this->escape($status['title']); ?></p>
          <p><?php echo nl2br($this->escape($status['post'])); ?></p><br>
          <p><span class="letterlittlesmall">カテゴリー：</span><?php echo $this->escape($status['category']); ?></p>
          <p><span class="letterlittlesmall" id="error<?php echo $status['id']?>" >投稿日時：</span><?php echo $this->escape($status['created_date']); ?></p>

        <?php if($status['posts_user_id']==$user['id']): ?>
          <div class="postdelete deletebtn">
            <form method="post" action="<?php echo $base_url; ?>/post/postdelete " ;>
              <input type="hidden" name="delete_id" value= "<?php echo $status['id']?>">
              <button class="postdeletegarbage" type="submit" name="delete" onclick="return doublecheck();" >
                <i class="far fa-trash-alt" aria-hidden="true"> 削除</i>
              </button>
            </form>
          </div>



        <?php endif; ?>
      </div>
      <br>

          <div class="comment_box">
            <ul class="comment_error_list">
              
              <?php if(isset($errors) && count($errors) >0): ?>
                <?php if($post_id == $status['id']): ?>
                  <?php foreach ($errors as $error):?>
                    <span class="commenterrorletterred"><li><?php echo $error?></li></span>
                  <?php endforeach; ?>
                <?php endif; ?>
              <?php endif; ?>
            </ul>
            <form action="<?php echo $base_url; ?>/status/comment" method="post">
              <input type="hidden" name="_token" value="<?php echo $this->escape($_token); ?>">
              <input type="hidden" name="post_id" value="<?php echo $this->escape($status['id']) ?>">
              <?php //login している人が投稿した　user_idに入る ?>
              <p>コメント <span class="lettersmall">(コメントは500文字以内)</span>
<textarea name="comment" rows="2" cols="60" ><?php if($status['id']==$post_id): ?><?php echo $this->escape($holdingcomment); ?><?php endif; ?></textarea></p>
              <p class="commentbtn">
                <input type="submit" value="コメント" id="<?php echo $status['id']?>">
              </p>
            </form>
          </div>

          <?php foreach($comments as $comment): ?>
            <?php if($comment['post_id'] == $status['id']): ?>
              <div class="comment_content" >
                <p><?php echo $this->escape($comment['name']); ?>さんからのコメント</p>
                <p><?php echo nl2br($this->escape($comment['comment'])); ?></p>
                <p><span class="letterlittlesmall">日付：</span><?php echo $this->escape($comment['created_date']); ?></p>
                <?php if($comment['user_id']==$user['id']): ?>
                  <div class="commentdelete deletebtn">
                    <form method="post" action="<?php echo $base_url; ?>/status/comment/commentdelete">
                      <input type="hidden" name="delete_id" value= "<?php echo $comment['id']?>">
                      <button class="commentdeletegarbage" type="submit" name="delete" onclick="return doublecheck();">
                        <i class="far fa-trash-alt" aria-hidden="true"> 削除</i>
                      </button>
                    </form>
                  </div>
                <?php endif; ?>
              </div>
            <?php endif; ?>
          <?php endforeach; ?>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
  <div class="Bob">
    <img src="businessman1-1.jpg"  width="15%" height="auto">
  </div>
  <div class="Alex">
    <img src="businessman2-1.jpg"  width="12%" height="auto">
  </div>
  </body>
  <div class="justblack">
  </div>
