<?php
class BbsApplication extends Application{
  protected $login_action = array ('account', 'signin');
  public function getRootDir(){
    return dirname(__FILE__);
  }
  protected function registerRoutes(){
    return array(
      '/' => array('controller'=> 'status','action' => 'index'),
      '/status/post' => array('controller' => 'post','action' => 'post'),
      '/status/search' => array('controller' => 'status','action' => 'search'),
      '/status/comment' => array('controller' => 'status','action' => 'comment'),
      '/status/comment/:action' => array('controller' => 'post'),
      // '/account' => array('controller'=> 'account','action' => 'signin'),
      '/account/:action' => array('controller' => 'account'),

      '/post' => array('controller'=> 'post','action' => 'index'),
      '/post/:action' => array('controller'=> 'post'),
      //StatusControllerの他のルーティング
      '/user/:login_id' => array('controller'=> 'status','action' => 'user'),
      '/user/:login_id/status/:id' =>array('controller'=> 'status','action' => 'show'),
      '/management' => array('controller'=> 'management','action' => 'index'),
      '/management/:action' => array('controller'=> 'management'),
      '/edit' => array('controller'=> 'edit','action' => 'index'),
      '/edit/:action' => array('controller'=> 'edit'),
      // AccountControllerのルーティング
      '/follow' => array('controller' => 'account','action' => 'follow'),
    );
  }
  protected function configure(){
    $this->db_manager->connect('master',array(
      'dsn' =>'mysql:dbname=fukuda_tomoki;host=localhost',
      'user'=>'root',
      'password'=>'22375015',
    ));
  }

}
?>
