
<?php
class ManagementRepository extends DbRepository{
  // public function fetchAllUsersData(){
  //   $sql="SELECT users.* FROM users ";
  // return $this->fetchAll($sql);
  // }

  public function fetchAllUsersDataInnerJoinedByBranchesAndDepartments(){
    $sql="SELECT users.id,users.login_id,users.name,
          branches.name as branches_name,
          departments.name as departments_name,
          users.is_deleted
          FROM fukuda_tomoki.users
          INNER JOIN fukuda_tomoki.branches ON users.branch_id = branches.id
          INNER JOIN fukuda_tomoki.departments ON users.department_id = departments.id";
  return $this->fetchAll($sql);
  }
  public function is_deletedUpdate($is_deleted,$user_id){
    $sql="  UPDATE	users
            SET	is_deleted=:is_deleted
            WHERE	users.id =:user_id ";
            $stmt = $this->execute($sql,array(
              ':is_deleted' => $is_deleted,
              ':user_id'=>$user_id
            ));
  }
}
?>
