<?php
class UserRepository extends DbRepository{
  public function insert($login_id,$password,$name,$branch_id,$department_id){
    $password=$this->hashPassword($password);
    $now = new DateTime('', new DateTimeZone('Asia/Tokyo'));
    // if($is_deleted=null){
    //   $state=0;
    // }else{
    //   $state=$is_deleted;
    // }
    $sql="INSERT INTO users(login_id,password,name,branch_id,department_id,created_date,is_deleted)
          VALUES(:login_id,:password,:name,:branch_id,:department_id,:created_date,:is_deleted)";

    $stmt = $this->execute($sql,array(
      ':login_id' => $login_id,
      ':password' => $password,
      ':name'=> $name,
      ':branch_id'=> $branch_id,
      ':department_id' =>$department_id,
      ':created_date' => $now->format('Y-m-d H:i:s'),
      ':is_deleted'=> 0,
    ));
  }

  public function hashPassword($password){
    return sha1($password . 'SecretKey');
  }

  public function fetchByLoginId($login_id){
    $sql = "SELECT * FROM users WHERE login_id = :login_id";
    return $this->fetch($sql, array(':login_id' => $login_id));
  }




  public function isUniqueLoginId($login_id){
    $sql = "SELECT COUNT(id) as count FROM users WHERE login_id = :login_id";
    $row = $this->fetch($sql,array(':login_id'=> $login_id));
    if($row['count']==='0'){
      return true;
    }
    return false;
  }
//   public function fetchAllFollowingsByUserId($user_id){
//     $sql = "SELECT u.* FROM user u LEFT JOIN following f ON f.following_id = u.id WHERE f.user_id =:user_id";
//     return $this->fetchAll($sql,array('user_id' => $user_id));
//   }


public function updateWithoutPassword($user_id,$login_id,$name,$branch_id,$department_id){

  // if($is_deleted=null){
  //   $state=0;
  // }else{
  //   $state=$is_deleted;
  // }

$sql="  UPDATE	users
        SET	login_id =:login_id,name=:name,branch_id=:branch_id,department_id=:department_id
        WHERE	users.id =:user_id ";
  // $sql="INSERT INTO users(login_id,password,name,branch_id,department_id)
  //       VALUES(:login_id,:password,:name,:branch_id,:department_id)";

  $stmt = $this->execute($sql,array(
    ':login_id' => $login_id,
    ':name'=> $name,
    ':branch_id'=> $branch_id,
    ':department_id' =>$department_id,
    ':user_id'=>$user_id,
  ));
}


public function update($user_id,$login_id,$password,$name,$branch_id,$department_id){
  $password=$this->hashPassword($password);
  // if($is_deleted=null){
  //   $state=0;
  // }else{
  //   $state=$is_deleted;
  // }

$sql="  UPDATE	users
        SET	login_id =:login_id,password=:password,name=:name,branch_id=:branch_id,department_id=:department_id
        WHERE	users.id =:user_id ";
  // $sql="INSERT INTO users(login_id,password,name,branch_id,department_id)
  //       VALUES(:login_id,:password,:name,:branch_id,:department_id)";

  $stmt = $this->execute($sql,array(
    ':login_id' => $login_id,
    ':password' => $password,
    ':name'=> $name,
    ':branch_id'=> $branch_id,
    ':department_id' =>$department_id,
    ':user_id'=>$user_id,
  ));
}



public function fetchAllUserIdFromUsers(){
  $sql="SELECT id FROM users ORDER BY created_date DESC";
  return $this->fetchAll($sql,array());
}


public function fetchEditUserById($id){
  $sql = "SELECT * FROM users WHERE id = :id";
  return $this->fetch($sql, array(':id' => $id));
}

}
 ?>
