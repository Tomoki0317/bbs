<?php
class CommentRepository extends DbRepository{
  public function insert($user_id, $post_id,$comment){
    $now = new DateTime('', new DateTimeZone('Asia/Tokyo'));
    $sql = "INSERT INTO comments (user_id,post_id,comment,created_date)  VALUES (:user_id, :post_id, :comment, :created_date)";
    $stmt = $this->execute($sql,array(
      ':user_id' => $user_id,
      ':post_id' => $post_id,
      ':comment' => $comment,
      ':created_date' => $now->format('Y-m-d H:i:s')
    ));
  }

  public function fetchAllCommentForThePostByUserId($user_id){
    $sql="SELECT comments.*, users.name FROM comments INNER JOIN users ON comments.user_id =users.id
          ORDER BY comments.created_date DESC";
    return $this->fetchAll($sql,array(':login_id' => $user_id));
  }
  public function deleteComment($delete_id){
    $sql="DELETE FROM comments WHERE comments.id=:delete_id";
    $stmt = $this->execute($sql,array(
      ":delete_id" => $delete_id

    ));
  }
    public function deleteCommentFromPostDelete($delete_id){
      $sql="DELETE FROM comments WHERE comments.post_id=:delete_id";
      $stmt = $this->execute($sql,array(
        ":delete_id" => $delete_id

    ));
  }
}

 ?>
