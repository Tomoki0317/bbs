<?php
class PostRepository extends DbRepository {
  public function insert($user_id, $title,$post,$category){
    $now = new DateTime('', new DateTimeZone('Asia/Tokyo'));
    $sql = "INSERT INTO posts (user_id,title,post,category,created_date)  VALUES
    (:user_id, :title, :post, :category, :created_date)";
    $stmt = $this->execute($sql,array(
      ':user_id' => $user_id,
      ':title' => $title,
      ':post' => $post,
      ':category' => $category,
      ':created_date' => $now->format('Y-m-d H:i:s')
    ));
  }

  public function fetchAllPersonalArchivesByUserId($user_id){
    $sql="SELECT users.id as users_id,
          users.login_id,
          users.name,
          users.is_deleted,
          posts.created_date,
          posts.id,
          posts.user_id as posts_user_id,
          posts.title,
          posts.post,
          posts.category
     FROM posts INNER JOIN users ON posts.user_id =users.id ORDER BY created_date DESC";
    return $this->fetchAll($sql,array());
  }

  public function fetchAllByUserId($user_id){
    $sql ="SELECT posts.*, users.login_id FROM posts INNER JOIN users ON posts.user_id = users.id
          WHERE users.id= :login_id ORDER BY posts.created_date DESC";
    return $this->fetchAll($sql,array(':login_id' => $user_id));
  }

  public function fetchByIdAndUserName($id, $user_id){
    $sql ="SELECT posts.*, users.login_id FROM posts LEFT JOIN users ON users.id = posts.user_id
          WHERE posts.id= :id AND users.login_id =:login_id ";
    return $this->fetch($sql,array(
      ':id' => $id,
      ':user_id' => $user_id,
    ));
  }

  public function deletePost($delete_id){
    $sql="DELETE FROM posts WHERE posts.id=:delete_id";
    $stmt = $this->execute($sql,array(
      ":delete_id" => $delete_id
    ));
  }

  // public function fetchAllCommentsByCategory_search($search_by_category){
  //   $sql="SELECT users.id as users_id,
  //         users.login_id,
  //         users.name,
  //         users.is_deleted,
  //         posts.created_date,
  //         posts.id,
  //         posts.user_id as posts_user_id,
  //         posts.title,
  //         posts.post,
  //         posts.category
  //         FROM posts
  //         INNER JOIN users ON posts.user_id =users.id
  //         where category LIKE '%$search_by_category%'
  //         ORDER BY created_date DESC";
  //
  //
  //   return $this->fetchAll($sql,array());
  // }
  public function fetchAllCommentsAfterSearching($search,$from,$to){
    $sql="SELECT users.id as users_id,
          users.login_id,
          users.name,
          users.is_deleted,
          posts.created_date,
          posts.id,
          posts.user_id as posts_user_id,
          posts.title,
          posts.post,
          posts.category
          FROM posts
          INNER JOIN users ON posts.user_id =users.id
          where :datefrom <= posts.created_date
          and posts.created_date <= :deteto ";
      if($search!=null){
        $sql .= "AND category LIKE :search ";
      }
      $sql .= "ORDER BY posts.created_date DESC";
      if($search!=null){
      return $this->fetchAll($sql,array(
        ':search' => $search,
        ':datefrom' => $from,
        ':deteto' => $to
        ));
      }
      return $this->fetchAll($sql,array(
      ':datefrom' => $from,
      ':deteto' => $to
    ));
  }
}
?>
